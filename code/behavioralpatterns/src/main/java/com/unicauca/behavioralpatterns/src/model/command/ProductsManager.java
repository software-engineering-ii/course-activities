/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicauca.behavioralpatterns.src.model.command;

/**
 *
 * @author sahydo
 */
public class ProductsManager {
    private ICommand command;
    
    public void setCommand(ICommand command) {
        this.command = command;
    }
    
    public void process(){
        if(command != null) {
            command.execute();
        }
    }
    
    public void undo(){
        if(command != null) {
            command.undo();
        }
    }
}
