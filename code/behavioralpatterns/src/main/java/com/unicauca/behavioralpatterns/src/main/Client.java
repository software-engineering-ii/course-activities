/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicauca.behavioralpatterns.src.main;

import com.unicauca.behavioralpatterns.src.model.command.AddCommand;
import com.unicauca.behavioralpatterns.src.model.command.ProductsManager;
import com.unicauca.behavioralpatterns.src.model.command.RemoveCommand;
import com.unicauca.behavioralpatterns.src.model.cor.Middleware;
import com.unicauca.behavioralpatterns.src.model.cor.RoleCheckMiddleware;
import com.unicauca.behavioralpatterns.src.model.entities.Ingredient;
import com.unicauca.behavioralpatterns.src.model.entities.Product;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import com.unicauca.behavioralpatterns.src.model.cor.Server;
import com.unicauca.behavioralpatterns.src.model.cor.ThrottlingMiddleware;
import com.unicauca.behavioralpatterns.src.model.cor.UserExistsMiddleware;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sahydo
 */
public class Client {

    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private static void commandDemo() {
        Product product1 = new Product("id1", "product name1", "description1", "100");
        Product product2 = new Product("id2", "product name2", "description2", "100");

        Ingredient ingredient1 = new Ingredient("id1", "ingredient name1", "description1", "unit1");
        Ingredient ingredient2 = new Ingredient("id2", "ingredient name2", "description2", "unit2");
        Ingredient ingredient3 = new Ingredient("id3", "ingredient name3", "description3", "unit3");
        Ingredient ingredient4 = new Ingredient("id4", "ingredient name4", "description4", "unit4");

        ArrayList<Ingredient> ingredientsForProduct1 = new ArrayList<>();
        ingredientsForProduct1.add(ingredient1);
        ingredientsForProduct1.add(ingredient2);
        ArrayList<Ingredient> ingredientsForProduct2 = new ArrayList<>();
        ingredientsForProduct2.add(ingredient1);
        ingredientsForProduct2.add(ingredient3);
        ingredientsForProduct2.add(ingredient4);

        ProductsManager manager = new ProductsManager();
        System.out.println("----------- Adding ingredients to Product 1 -----------");
        manager.setCommand(new AddCommand(product1, ingredientsForProduct1));
        manager.process();
        product1.showIngredients();
        System.out.println("----------- Adding ingredients to Product 2 -----------");
        manager.setCommand(new AddCommand(product2, ingredientsForProduct2));
        manager.process();
        product2.showIngredients();

        System.out.println("----------- Ingredients in products -----------");
        ingredient1.showProducts();
        ingredient2.showProducts();

        System.out.println("----------- Removing ingredients for Product 2 -----------");
        manager.setCommand(new RemoveCommand(product2, ingredientsForProduct1));
        manager.process();
        product2.showIngredients();

    }

    private static void corDemo() {
        Server server = new Server();
        server.register("admin@example.com", "admin_pass");
        server.register("user@example.com", "user_pass");

        // All checks are linked. Client can build various chains using the same
        // components.
        Middleware middleware = new ThrottlingMiddleware(2);
        middleware.linkWith(new UserExistsMiddleware(server))
                .linkWith(new RoleCheckMiddleware());
        // Server gets a chain from client code.
        server.setMiddleware(middleware);
        try {
            System.out.print("Enter an email: ");
            String email = reader.readLine();
            System.out.print("Enter an password: ");
            String password = reader.readLine();
            boolean result = server.logIn(email, password);
            System.out.println("Result: " + result);
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            boolean close = false;
            do {
                System.out.print("Enter pattern name (or exit): ");
                String patternName = reader.readLine();
                switch (patternName) {
                    case "Command" ->
                        commandDemo();
                    case "CoR" ->
                        corDemo();
                    case "exit" ->
                        System.exit(0);
                    default ->
                        System.out.println("Pattern name not recognized");
                }
            } while (!close);
        } catch (IOException e) {
            System.out.println(e);
        }
    }

}
