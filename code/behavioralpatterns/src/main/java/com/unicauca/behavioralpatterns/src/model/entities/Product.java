/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicauca.behavioralpatterns.src.model.entities;

import java.util.ArrayList;
import java.util.Optional;

/**
 *
 * @author sahydo
 */
public class Product {

    private String id;
    private String name;
    private String description;
    private String price;
    private final ArrayList<Ingredient> ingredients = new ArrayList<>();

    public Product(String id, String name, String description, String price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void addIngredientsToProduct(ArrayList<Ingredient> ingredients) {
        this.ingredients.addAll(ingredients);
    }

    public void removeIngredientsFromProduct(ArrayList<Ingredient> ingredients) {
        ingredients.forEach(ingredient -> {
            Optional<Ingredient> result = this.ingredients.stream().filter(ing -> ing.getId().equals(ingredient.getId())).findFirst();
            if (result.isPresent()) {
                this.ingredients.removeIf(ing -> ing.getId().equals(result.get().getId()));
            }
        });
    }
    
    public void showIngredients() {
        System.out.println("-------- Ingredients in product: " + this.name + " --------");
        this.ingredients.forEach(ingredient -> {
            System.out.println(ingredient.toString());
        });
    }
    
    @Override
    public String toString() {
        return "Id: " + this.id + ", Name: " + this.name + ", Description: " + this.description;
    }
}
