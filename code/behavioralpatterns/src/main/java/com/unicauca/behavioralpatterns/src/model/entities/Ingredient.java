/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicauca.behavioralpatterns.src.model.entities;

import java.util.ArrayList;
import java.util.Optional;

/**
 *
 * @author sahydo
 */
public class Ingredient {

    private String id;
    private String name;
    private String description;
    private String unit;
    private final ArrayList<Product> products = new ArrayList<>();

    public Ingredient(String id, String name, String description, String unit) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.unit = unit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void addProductToIngredient(Product product) {
        this.products.add(product);
    }

    public void removeProductForIngredient(Product product) {
        Optional<Product> result = this.products.stream().filter(ing -> ing.getId().equals(product.getId())).findFirst();
        if (result.isPresent()) {
            this.products.removeIf(ing -> ing.getId().equals(result.get().getId()));
        }
    }

    public void showProducts() {
        System.out.println("-------- Products with ingredient: " + this.name + " --------");
        this.products.forEach(product -> {
            System.out.println(product.toString());
        });
    }

    @Override
    public String toString() {
        return "Id: " + this.id + ", Name: " + this.name + ", Description: " + this.description;
    }

}
