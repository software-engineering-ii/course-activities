/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicauca.behavioralpatterns.src.model.command;

import com.unicauca.behavioralpatterns.src.model.entities.Ingredient;
import com.unicauca.behavioralpatterns.src.model.entities.Product;
import java.util.ArrayList;

/**
 *
 * @author sahydo
 */
public class AddCommand implements ICommand {

    private Product product;
    private ArrayList<Ingredient> ingredients;

    public AddCommand(Product product, ArrayList<Ingredient> ingredients) {
        this.product = product;
        this.ingredients = ingredients;
    }

    @Override
    public void execute() {
        product.addIngredientsToProduct(ingredients);
        ingredients.forEach(ingredient -> {
            ingredient.addProductToIngredient(product);
        });
    }

    @Override
    public void undo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
