/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicauca.practice.junit_git.src.sorting.util;

/**
 *
 * @author kevit
 */
public class InsertionSort extends SortingUtil{

    public InsertionSort(String type) {
        super(type);
        System.out.println("Ordering with " + this.getClass().getSimpleName());
    }
    @Override
    public int[] sort(int[] array) {
        int p, j;
        int aux;
        for (p = 1; p < array.length; p++) { // desde el segundo elemento hasta
            aux = array[p];           // el final, guardamos el elemento y
            j = p - 1;            // empezamos a comprobar con el anterior
            while ((j >= 0) && (aux < array[j])) { // mientras queden posiciones y el                                
                // valor de aux sea menor que los
                array[j + 1] = array[j];   // de la izquierda, se desplaza a
                j--;               // la derecha
            }
            array[j + 1] = aux;       // colocamos aux en su sitio
        }
        return array;
    }
    
}
