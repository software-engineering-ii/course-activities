/**
 * 
 */
package com.unicauca.products.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unicauca.products.access.dao.IProductDao;
import com.unicauca.products.domain.entity.Product;

/**
 * @author sahydo, ahurtado, wpantoja
 *
 */
@Service
public class ProductServiceImpl implements IProductService {

	@Autowired
	private IProductDao productDao;

	/**
	 * Servicio para buscar todos los productos
	 *
	 * @return Listado de productos
	 */
	@Override
	@Transactional(readOnly = true) // Para que esté sincronizada con la bd
	public List<Product> findAll() {
		return (List<Product>) productDao.findAll();
	}

	/**
	 * Busca un producto por su Id
	 *
	 * @param id identificador del producto
	 * @return objeto de tipo producto
	 */
	@Override // Para que esté sincronizada con la bd
	public Product find(Long id) {
		Product prod = productDao.findById(id).orElse(null);
		return prod;
	}

	/**
	 * Crea un nuevo producto
	 *
	 * @param product producto a crear en la bd
	 * @return Producto creado
	 */
	@Override
	@Transactional
	public Product create(Product product) {
		return productDao.save(product);
	}

	/**
	 * Modifica o edita un producto
	 *
	 * @param id,     identificador del producto a modificar
	 * @param product producto con los datos a editar
	 * @return Producto modificado
	 */
	@Override
	@Transactional
	public Product update(Long id, Product product) {
		Product prod = this.find(id);
		prod.setName(product.getName());
		prod.setPrice(product.getPrice());
		return productDao.save(prod);
	}

	/**
	 * Eliminar producto por su id
	 *
	 * @param id identificador del producto a eliminar
	 */
	@Override
	@Transactional
	public void delete(Long id) {
		productDao.deleteById(id);
	}

}
