/**
 * 
 */
package com.unicauca.products.access.dao;

import org.springframework.data.repository.CrudRepository;

import com.unicauca.products.domain.entity.Product;

/**
 * @author sahydo, ahurtado, wpantoja
 *
 */
public interface IProductDao extends CrudRepository<Product, Long> {

}
