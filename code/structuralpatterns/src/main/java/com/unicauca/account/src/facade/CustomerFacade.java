/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicauca.account.src.facade;

import com.unicauca.account.src.model.customer.Account;
import com.unicauca.account.src.model.customer.Address;
import com.unicauca.account.src.model.customer.AddressState;
import com.unicauca.account.src.model.customer.CreditCard;
import com.unicauca.account.src.model.customer.CreditCardType;

/**
 *
 * @author sahydo
 */
public class CustomerFacade implements ICustomerFacade {
    private static CustomerFacade instance;

    private String firstName;
    private String lastName;
    
    private CreditCardType cardType;
    private String cardNumber;
    private String cardExpDate;

    private String address;
    private String city;
    private AddressState state;
    
    public static CustomerFacade getCustomerFacade(String firstName, String lastName, CreditCardType cardType, String cardNumber, String cardExpDate, String address, String city, AddressState state){
        if(instance == null) {
            instance = new CustomerFacade(firstName, lastName, cardType, cardNumber, cardExpDate, address, city, state);
        }
        return instance;
    }

    private CustomerFacade(String firstName, String lastName, CreditCardType cardType, String cardNumber, String cardExpDate, String address, String city, AddressState state) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.cardType = cardType;
        this.cardNumber = cardNumber;
        this.cardExpDate = cardExpDate;
        this.address = address;
        this.city = city;
        this.state = state;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public CreditCardType getCardType() {
        return cardType;
    }

    public void setCardType(CreditCardType cardType) {
        this.cardType = cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardExpDate() {
        return cardExpDate;
    }

    public void setCardExpDate(String cardExpDate) {
        this.cardExpDate = cardExpDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public AddressState getState() {
        return state;
    }

    public void setState(AddressState state) {
        this.state = state;
    }
    
    public boolean saveCustomerData(){
        boolean isValid = true;
        Address address = new Address(this.getAddress(), this.getCity(), this.getState());
        if (!address.isValid()) {
            System.out.println("Error creating Address");
            isValid = false;
        }
        CreditCard cCard = new CreditCard(this.getCardType(), this.getCardNumber(), this.getCardExpDate());
        if (!cCard.isValid()) {
            System.out.println("Error creating CreditCard");
            isValid = false;
        }
        Account account = new Account(this.getFirstName(), this.getLastName(), cCard);
        if (!account.isValid()) {
            System.out.println("Error creating Account");
            isValid = false;
        }
        System.out.println("Saving customer data");
        return isValid;
    }
    public boolean editCustomerData(String fName, String lName){
        System.out.println("Editing customer data");
        this.setFirstName(firstName);
        this.setLastName(lastName);
        return true;
    }
    public boolean verifyCustomerData(){
        System.out.println("verifying customer data");
        return true;
    }
    
    
}
