/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicauca.account.src.facade;

/**
 *
 * @author sahydo
 */
public interface ICustomerFacade {
    public abstract boolean saveCustomerData();
    public abstract boolean editCustomerData(String fName, String lName);
    public abstract boolean verifyCustomerData();
}
