/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicauca.account.src.model.security;

/**
 *
 * @author sahydo
 */
public enum Role {
    ADMIN,
    CLIENT,
    MANAGER
}
