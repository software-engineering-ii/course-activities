/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicauca.account.src.model.customer;

/**
 *
 * @author sahydo
 */
public class Account {

    private String firstName;
    private String lastName;
    private CreditCard creditCard;

    public Account(String fname, String lname, CreditCard cCard) {
        this.firstName = fname;
        this.lastName = lname;
        this.creditCard = cCard;
    }

    public boolean isValid() {
        /*
		 * Let's go with simpler validation here to keep the example simpler.
         */
        if (firstName.trim().equals("")) {
            return false;
        }
        else if (lastName.trim().equals("")) {
            return false;
        }
        return true;

    }

    public boolean save() {
        String dataLine = getLastName() + "," + getFirstName();
        System.out.println("Grabado cuenta: " + dataLine);
        return true;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public CreditCard getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }
    
}
