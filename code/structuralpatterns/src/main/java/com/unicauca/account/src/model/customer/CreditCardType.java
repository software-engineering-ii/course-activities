/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicauca.account.src.model.customer;

/**
 *
 * @author sahydo
 */
public enum CreditCardType {
    VISA,
    DISCOVER,
    MASTERCARD
}
