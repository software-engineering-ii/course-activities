/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicauca.account.src.facade.main;

import com.unicauca.account.src.facade.CustomerFacadeProxy;
import com.unicauca.account.src.facade.ICustomerFacade;
import com.unicauca.account.src.model.customer.AddressState;
import com.unicauca.account.src.model.customer.CreditCardType;
import com.unicauca.account.src.model.security.Role;

/**
 *
 * @author sahydo
 */
public class Client {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        ICustomerFacade proxy = new CustomerFacadeProxy("Fname", "Lname", CreditCardType.VISA, "1234567890987654", "2021", "2020", "mycity", AddressState.ACTIVE, Role.CLIENT);
        System.out.println("Result: " + proxy.saveCustomerData());
        System.out.println("Result: " + proxy.editCustomerData("My name", "My lastname"));
        System.out.println("Result: " + proxy.verifyCustomerData());
    }
    
}
