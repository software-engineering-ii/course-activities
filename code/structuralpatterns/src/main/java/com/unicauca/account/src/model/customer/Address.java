/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicauca.account.src.model.customer;

/**
 *
 * @author sahydo
 */

public class Address {

    private String address;
    private String city;
    private AddressState state;

    public Address(String add, String cty, AddressState st) {
        this.address = add;
        this.city = cty;
        this.state = st;
    }

    public boolean isValid() {
        /*
		 * The address validation algorithm could be complex in real-world
		 * applications. Let's go with simpler validation here to keep the
		 * example simpler.
         */
        if (getState() != AddressState.ACTIVE) {
            return false;
        }
        return true;
    }

    public boolean save() {
        String dataLine = getAddress() + "," + getCity() + "," + getState();
        System.out.println("Grabado Address: " + dataLine);
        return true;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public AddressState getState() {
        return state;
    }

    public void setState(AddressState state) {
        this.state = state;
    }
    
    
}
