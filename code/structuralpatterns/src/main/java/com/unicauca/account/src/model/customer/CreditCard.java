/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicauca.account.src.model.customer;

/**
 *
 * @author sahydo
 */
public class CreditCard {

    private CreditCardType cardType;
    private String cardNumber;
    private String cardExpDate;

    public CreditCard(CreditCardType ccType, String ccNumber, String ccExpDate) {
        cardType = ccType;
        cardNumber = ccNumber;
        cardExpDate = ccExpDate;
    }

    public boolean isValid() {
        /*
		 * Let's go with simpler validation here to keep the example simpler.
         */

        if (getCardType() == CreditCardType.VISA) {
            return (getCardNumber().trim().length() == 16);
        }
        if (getCardType()== CreditCardType.DISCOVER) {
            return (getCardNumber().trim().length() == 15);
        }
        if (getCardType() == CreditCardType.MASTERCARD) {
            return (getCardNumber().trim().length() == 16);
        }
        return false;
    }

    public boolean save() {
        String dataLine
                = getCardType() + "," + getCardNumber() + ","
                + getCardExpDate();
        System.out.println("Grabado CreditCard: " + dataLine);
        return true;
    }

    public CreditCardType getCardType() {
        return cardType;
    }

    public void setCardType(CreditCardType cardType) {
        this.cardType = cardType;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardExpDate() {
        return cardExpDate;
    }

    public void setCardExpDate(String cardExpDate) {
        this.cardExpDate = cardExpDate;
    }
    
}
