/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unicauca.account.src.facade;

import com.unicauca.account.src.model.customer.AddressState;
import com.unicauca.account.src.model.customer.CreditCardType;
import com.unicauca.account.src.model.security.Role;

/**
 *
 * @author iniciativa
 */
public class CustomerFacadeProxy implements ICustomerFacade {
    
    private ICustomerFacade realSubject;
    private Role role;

    public CustomerFacadeProxy(String firstName, String lastName, CreditCardType cardType, String cardNumber, String cardExpDate, String address, String city, AddressState state, Role role) {
        this.role = role;
        this.realSubject = CustomerFacade.getCustomerFacade("Fname", "Lname", CreditCardType.VISA, "1234567890987654", "2021", "2020", "mycity", AddressState.ACTIVE);
    }

    public ICustomerFacade getRealSubject() {
        return realSubject;
    }

    public void setRealSubject(ICustomerFacade realSubject) {
        this.realSubject = realSubject;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean saveCustomerData() {
        if (this.getRole() != Role.ADMIN) {
            System.out.println("User not allowed to perform this action");
            return false;
        }
        return this.getRealSubject().saveCustomerData();
    }

    @Override
    public boolean editCustomerData(String fName, String lName) {
        if (this.getRole() != Role.ADMIN && this.getRole() != Role.MANAGER) {
            System.out.println("User not allowed to perform this action");
            return false;
        }
        return this.getRealSubject().editCustomerData(fName, lName);
    }

    @Override
    public boolean verifyCustomerData() {
        if (this.getRole() != Role.CLIENT) {
            System.out.println("User not allowed to perform this action");
            return false;
        }
        return this.getRealSubject().verifyCustomerData();
    }
    
}
